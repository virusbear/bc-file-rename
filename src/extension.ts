import * as vscode from 'vscode';
import * as path from 'path';
import { print } from 'util';

var watcher: vscode.FileSystemWatcher;

export function activate(context: vscode.ExtensionContext) {

	const saveBeforeRename = vscode.workspace.getConfiguration('bcfilerename').get('saveBeforeRename');

	let single = vscode.commands.registerCommand('extension.renameSingleFile', () => {
		onBeforeRename().then(() => {
			let editor = vscode.window.activeTextEditor;
		
			if(editor) {
				processFile(editor.document.uri);
			}
		});
	});

	let folder = vscode.commands.registerCommand('extension.renameFolder', () => {
		onBeforeRename().then(() => {
			workspaceFolders((folders) => {
				if(folders.length > 1) {
					vscode.window.showQuickPick(folders.map((f) => f.uri.path), {canPickMany: true, placeHolder: 'Select folders to rename *.al files.'}).then((selection) => {
						if(selection) {
							processFolders(folders.filter((f) => f.uri.path in selection));
						}
					});
				} else if(folders.length === 1) {
					processFolder(folders[0]);
				}
			});
		});
	});

	let complete = vscode.commands.registerCommand('extension.renameAll', () => {
		onBeforeRename().then(() => {
			workspaceFolders((folders) => {
				processFolders(folders);
			});
		});
	});

	let startListen = vscode.commands.registerCommand('extension.startRenameOnSave', () => {
		workspaceFolders((folders) => {
			folders.forEach((folder) => {
				watcher = vscode.workspace.createFileSystemWatcher(new vscode.RelativePattern(folder, '**/*.al'));
				watcher.onDidCreate((file) => {
					processFile(file);
				});
				watcher.onDidChange((file) => {
					
					processFile(file);
				});
				
			});
		});
	});

	let stopListen = vscode.commands.registerCommand('extension.stopRenameOnSave', () => {
		if(watcher){
			watcher.dispose();
		}
	});

	context.subscriptions.push(single);
	context.subscriptions.push(folder);
	context.subscriptions.push(complete);
	context.subscriptions.push(startListen);
	context.subscriptions.push(stopListen);
}

export function deactivate() {}

function workspaceFolders(callback: (folders: vscode.WorkspaceFolder[]) => void) {
	let workspace = vscode.workspace;

	if(workspace) {
		let folders = workspace.workspaceFolders;
		
		if(folders) {
			callback(folders);
		}
	}
}

function processFolders(folders: vscode.WorkspaceFolder[]) {
	folders.forEach((folder) => {
		processFolder(folder);
	});
}

function processFolder(folder: vscode.WorkspaceFolder) {
	vscode.workspace.findFiles(new vscode.RelativePattern(folder, '**/*.al')).then((files) => {
		files.forEach((file) => {
			processFile(file);
		});
	});
}

async function processFile(file: vscode.Uri) {
	if(!file.path.endsWith('.al')) {
		return;
	}

	newUri(file, (uri) => {
		let we = new vscode.WorkspaceEdit();
		we.renameFile(file, uri, {overwrite: true});
		vscode.workspace.applyEdit(we);
	});
}

function newUri(file: vscode.Uri, callback: (newUri: vscode.Uri) => void) {
	vscode.workspace.openTextDocument(file).then((doc) => {
		let m = doc.getText().match(/^\w+\s\d+\s.+$|^\w+\s.+\s\w+.+$/m)
		if(m){
			let header = m[0].replace(/".*?"/g, (s) => {
				return s.replace(/\s/g, "").replace(/"/g, "");
			});

			let p = header.split(' ');
			let newPath = path.dirname(file.fsPath) + '\\' + type(p) + id(p) + '.' + name(p) + '.al';

			if(file.fsPath !== newPath){
				callback(vscode.Uri.file(newPath));
			}
		}
	});
}

function name(header: string[]): string {
	if(header.length === 0){
		return '';
	}

	return isNumber(header[1]) ? header[2] : header[1];
}

function id(header: string[]): string {
	if(header.length === 0 || !isNumber(header[1])){
		return '';
	}

	return header[1];
}

function type(header: string[]): string {
	if(header.length === 0 || header[0].length === 0){
		return '';
	}

	let objType = header[0].substr(0, 1).toUpperCase() + header[0].substr(1, 2);
	if(!(header.length < 3)) {
		if(header[3] === 'extends') {
			objType += 'Ext';
		} else  if(header[2] === 'customizes') {
			objType += 'Cust';
		}
	}

	return objType;
}

async function onBeforeRename(){
	vscode.workspace.saveAll();
}

function isNumber(val: string): boolean {
	return !isNaN(+val);
}